import java.util.Scanner;

public class Gravitacija {
    
    public static double izracunaj(int visina) {
            double v = (double)visina; //v kilometrih
    		double mZemlje = 5.972*1e24;
    		double c = 6.674*1e-11;
    		double r = 6.371*1e6;
    
    		double a = c*mZemlje/((r+v*1000)*(r+v*1000));
    		return a;
        }
        
    public static void izpis(double a, int visina) {
        System.out.println(visina);
        System.out.println(a);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int visina = sc.nextInt();
        
        izpis(izracunaj(visina), visina);
	}
}